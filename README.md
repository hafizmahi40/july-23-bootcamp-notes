# July-23-bootcamp-notes


#Getting Started
This is the first *git* repo for **July 23 Bootcamp**


## To Do Tasks

- [x] Learn Git 
- [] Sign up for Gitlab
- [] Sign up for TS4U Git
- [] Clone the Repo
- [] Deploy the Two-Tier Application

## How to Push to git repository
```
git clone
git add
git commit -m "commit message"
git push -u origin main
```
